from keras.layers import Input, Dense, Activation
import numpy as np
from keras.models import Model, load_model
from keras import optimizers, activations
import math
# Loading the autoencoder by autoencoder = load_model('MicroarrayDeep512_512_512_AE20K.h5').
# In case of loading the autoencoder MicroarrayDeep1024_1024_1024_AE20K.h5 and MicroarrayDeep256_256_256_AE20K.h5, we replaced indS = 512 to indS = 1024 and indS = 256 respectively.  
autoencoder = load_model('MicroarrayDeep512_512_512_AE20K.h5')
indS = 512
b = np.identity(indS, dtype = float) 
input_BC = Input(shape=(indS,))
laye3r = autoencoder.get_layer("dense_4")
L3 = laye3r(input_BC)
autoencodeL2r = Model(input_BC, L3)
yytrk = autoencodeL2r.predict(b)
reswf = np.transpose(yytrk)
# The reference file for the genes in which the autoencoder is trained, RefEntrezMicroarray.txt and RefEntrezRNAseq.txt correspond to micro-array and RNA-seq data respectively.  
ytre = np.genfromtxt('RefEntrezMicroarray.txt',dtype='str')
yytrv = np.argsort(-reswf, axis=0)
entNe = ytre[yytrv]
np.savetxt("EntrezCorrectH3.txt", entNe,delimiter='\t',fmt='%s')

