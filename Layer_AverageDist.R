rm(list = ls()) 
library(igraph)
aaa <- read.table("Entrez_PPi.txt",sep=",",header=T)
GrS <- graph.data.frame(unique(aaa) , directed = FALSE)
cwfeG <- simplify(GrS , remove.multiple = TRUE, remove.loops = TRUE)
xx <- distances(cwfeG)
ssqw <- unique(union(aaa$Entrez1,aaa$Entrez2))
llk <- (c(2,4,8,16,32,64,128,256))*50
p <- length(llk)
# Please put one of the EntrezCorrectH1.txt, EntrezCorrectH2.txt and EntrezCorrectH3.txt for the first, second and third hidden layer of deepAE with 512 nodes  
acc <-  read.table("EntrezCorrectH1.txt")
outputp <- matrix(NA, p,dim(acc)[2]) # storage matrix
cc <- 1
for (jxt in llk)
{
	for (ixx in 1:dim(acc)[2])
	{
		xeIDx <- acc[ ,ixx]
		xeIDx1 <- xeIDx[xeIDx%in%ssqw]
		flis <- as.character(xeIDx1[1:jxt])
		yy <- xx[row.names(xx) %in% flis, ]
		zz <- yy[,colnames(xx) %in% flis ]
		vvzz <- zz[upper.tri(zz)]
		Hdisv  <- 1/(mean(1/vvzz))
		outputp[cc,ixx] <- Hdisv
	}
cc <- cc + 1
}
# Please rename LightuFirst512.txt, acrodding to the hidden layer of the deepAE
write.table(t(outputp),"LightuFirst512.txt",sep="\t",col.names=F,row.names=F)

